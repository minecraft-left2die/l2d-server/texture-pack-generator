# L2D Texture Pack Generator

This repo contains some base files from the [Vanilla Tweaks](https://vanillatweaks.net) project and a script to grab other packs and add in pieces used on L2D.

## Usage

Running the `gen-pack.sh` script will create an `l2d.zip` resource pack file.

The script will download the Faithful texture pack, merge in the changes stored in this repo, and merge in any changes noted in `packs.conf`.

Note: Any pack URL in `packs.conf` that is located at `https://minecraftleft2die.com/files87469/` is access restricted to only Gitlab's build process to avoid distribution licensing issues.  To build with these packs included yourself, a separate URL will need provided.  MediaFire direct links expire after so much time, hence why they have to be hosted like this (but can be used for testing and local building).


## Contributing

Any contribution must follow these guidelines:

1) Third party packs must get added to `packs.conf` listing each specific file to copy.  Only third party packs that support the newer pack format and naming conventions can be used (unless you also update the script to convert).  The source URL must be an official source URL and one that won't change (like Mediafire's do).  If the official source cannot be used, the pack can be hosted in a directory locked down to only Gitlab-only access hosted by Minecraft Left2Die.
2) The only exception to #1 is graphics from any pack that allows for partial distribution and graphic usage in other packs.  That pack's license along with a list of included files will need copied into the `base` directory along with any graphics (similar to the Vanilla Tweaks setup).
3) Pack pieces from Vanilla Tweaks can be committed directly in this repo in the `base` directory.  Update the `base/VanillaTweaksCredit.txt` file with any new files added.
4) Any pack graphics you personally design can be committed directly into this repo in the `base` directory, however, by doing so you grant Minecraft Left2Die irrevocable rights to use these graphics not necessarily restricted to use in the resource pack.

