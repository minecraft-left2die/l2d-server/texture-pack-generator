#!/bin/bash

SCRIPTDIR="$( cd "$( dirname "$0" )" && pwd )"
PACKDIR="${SCRIPTDIR}/pack"
TMPDIR="${SCRIPTDIR}/tmp"

declare -A PACKS
# shellcheck source=./packs.conf
source "$SCRIPTDIR"/packs.conf || exit 1

echo "Copy base directory to $PACKDIR"
cp -a "${SCRIPTDIR}"/base "$PACKDIR" || exit 1

if [[ ! -d $TMPDIR ]]; then
  echo "Creating $TMPDIR"
  mkdir -v "$TMPDIR" || exit 1
fi

echo "Downloading base Faithful pack"
wget -O faithfulvenom.zip "https://drive.google.com/uc?export=download&id=1pn_KGHqS5Dd0zqtMElyF1mYPkXIPmzgj" || exit 1

echo "Unzipping Faithful pack"
unzip -q -n faithfulvenom.zip -d "$PACKDIR" || exit 1

echo "Deleting unneeded files from Faithful"
rm -rf "${PACKDIR}"/assets/{fabric,forge,modmenu,realms} || exit 1
rm -rf "${PACKDIR}"/assets/minecraft/{optifine,sounds} || exit 1

IFS=$'\n'
for i in "${!PACKS[@]}"; do
  echo "Downloading extra pack from $i"
  wget -O "${SCRIPTDIR}/pack.zip" "$i" || exit 1

  echo "Unzipping extra pack"
  unzip -q -d "$TMPDIR" "${SCRIPTDIR}/pack.zip" || exit 1

  echo "Copying files from extra pack"
  for j in ${PACKS[$i]}; do
    SRCFILE="${j:3:-1}"
    DSTFILE=${SRCFILE//items/item}
    cp -v "${TMPDIR}/${SRCFILE}" "${PACKDIR}/${DSTFILE#*/}" || exit 1
  done
  echo "Deleting extra pack"
  rm -fv "${SCRIPTDIR}/pack.zip" || exit 1
done

echo "Zipping up the L2D pack"
cd "$PACKDIR" || exit 1
zip -q -r "${SCRIPTDIR}/l2d.zip" ./* || exit 1

echo "Cleaning up"
rm -rf "$TMPDIR" "$PACKDIR" "${SCRIPTDIR}"/faithfulvenom.zip || exit 1

